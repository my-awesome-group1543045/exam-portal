import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  constructor(private userService:UserService, private snack:MatSnackBar) { }

 public user = {
   username:'',
   password:'',
   firstName:'',
   lastName:'',
   email:'',
   phone:'',
 };
 
  ngOnInit() : void{}

  formSubmit() {
    
    if(this.user.username==''||this.user.username==null){
      // alert('Username is required');
      this.snack.open("Username is required !!",'',{
        duration:2000,
        verticalPosition:'top',
        panelClass:"red"
      });
       return;
     
    }

    this.userService.addUser(this.user).subscribe(
      (data)=>{
        console.log("Data", data);
        // alert('Sign up successful!');
        Swal.fire('Success','User id is ','success');
      },
      (error) =>{
        console.error("Error", error);
        //alert('Something went wrong, please try again later.');
        Swal.fire('Error','try again some Time later','error');
        this.snack.open("User Is already exist, please try again.",'',{
          duration:3000,
      }
    );
      }
    )
  }
  
}

